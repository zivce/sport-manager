const gulp = require('gulp');
const browserify = require("browserify");
const source = require('vinyl-source-stream');
const tsify = require("tsify");
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const cleanCSS = require('gulp-clean-css');
const del = require('del');
const merge = require('merge-stream');

const paths = {
    typescriptEntries: ['js/event-calendar.ts',
        'js/login.ts', 'js/event-type-validation.ts',
        'js/navigation.ts', 'js/users-control-panel.ts',
        'js/event-type-list.ts'],
    srcCodeDir: './src/main/resources/static/admin_root/',
    distDir: {
        root: 'dist',
        js: 'dist/js'
    }
};

const regexPatterns = {
    scssMatcher: paths.srcCodeDir + '**/*.scss',
    tsMatcher: paths.srcCodeDir + '**/*.ts',
    distDirMatcher: paths.srcCodeDir + 'dist/**'
}

gulp.task('copy-assets-to-public', function () {
    return gulp.src(paths.srcCodeDir + 'assets/**')
        .pipe(gulp.dest(paths.srcCodeDir + paths.distDir.root + '/assets'));
})

const compileTypescriptTask = function () {
    const entries = paths.typescriptEntries;
    return merge(entries.map(entry => browserify({
        basedir: paths.srcCodeDir,
        debug: true,
        entries: entry,
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .bundle()
        .pipe(source(entry.slice(0, -3) + '.js'))
        .pipe(gulp.dest(paths.srcCodeDir + paths.distDir.root))));
};

gulp.task('compile-typescript', compileTypescriptTask);

gulp.task('compile-typescript:watch', function () {
    gulp.watch(regexPatterns.tsMatcher, compileTypescriptTask);
});

const sassTask = function () {
    return gulp.src(regexPatterns.scssMatcher)
        .pipe(sass({
            includePaths: ['node_modules']
        }).on('error', sass.logError))
        .pipe(cleanCSS({ inline: ['local'] }))
        .pipe(gulp.dest(paths.srcCodeDir + paths.distDir.root));
}

gulp.task('sass', sassTask);

gulp.task('sass:watch', function () {
    gulp.watch(regexPatterns.scssMatcher, sassTask);
});

const cleanBuildDir = function () {
    return del([regexPatterns.distDirMatcher]);
}

gulp.task('build:clean', cleanBuildDir);

gulp.task('default', gulp.parallel('compile-typescript', 'copy-assets-to-public',
    'sass', 'compile-typescript:watch', 'sass:watch'));
