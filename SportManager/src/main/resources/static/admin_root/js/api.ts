export default {
    PARTICIPATE_URL : "http://localhost:8080/api/events/participate",
    EVENTS_URL : "http://localhost:8080/api/events/",
    EVENT_TYPES_URL : "http://localhost:8080/api/event-types/",
    USERS_URL : "http://localhost:8080/api/users",
    USER_URL : "http://localhost:8080/users-control-panel/username"
}