package com.sportmanager.SportManager.RestControllers;

import java.util.HashSet;

import javax.validation.Valid;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.view.RedirectView;

import com.sportmanager.SportManager.Models.ConfirmationToken;
import com.sportmanager.SportManager.Models.Role;
import com.sportmanager.SportManager.Models.User;
import com.sportmanager.SportManager.Services.ConfirmationTokenRepository;
import com.sportmanager.SportManager.Services.MailService;
import com.sportmanager.SportManager.Services.UserService;
import com.sportmanager.SportManager.util.GenericResponse;

import DTOs.RegisterUserDto;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class ClientRestController {
	private final UserService userService;
	private final MailService mailService;
	private final ConfirmationTokenRepository confirmationTokenRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	@PostMapping(path = "/login")
	public ResponseEntity<String> loginClient(@RequestParam("email") String email,
			@RequestParam("password") String password) {
		ResponseEntity<String> resp = new ResponseEntity<String>(HttpStatus.OK);
		boolean userFound = userService.findUserByEmail(email) != null;

		if (!userFound) {
			resp.status(HttpStatus.BAD_REQUEST);
			return resp;
		}
		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();

		// boolean passwordCorrect =

		return null;
	}

	@PostMapping(path = "/register")
	public ResponseEntity<String> registerClient(@RequestBody @Valid RegisterUserDto registerUserDto,
			BindingResult result, WebRequest request, Errors errors) {

		if (result.hasErrors()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid input");
		}

		User existingUser = userService.findUserByEmail(registerUserDto.getEmail());

		if (existingUser != null) {
			return ResponseEntity.status(HttpStatus.OK).body("Email is already in use");
		} else {

			User user = userService.registerNewUserAccount(registerUserDto);

			ConfirmationToken confirmationToken = new ConfirmationToken(user);
			confirmationTokenRepository.save(confirmationToken);

			String host = "http://localhost:8080/client/";
			String confirmationUrl = host + "confirm-register?token=" + confirmationToken.getConfirmationToken();
			try {
				mailService.sendMessage(user.getEmail(), "Complete Registration!",
						"To confirm your account, please click here : " + confirmationUrl);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return ResponseEntity.status(HttpStatus.OK)
					.body("Registration completed successfully. Please check your email for email confirmation");
		}
	}

}
