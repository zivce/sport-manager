package com.sportmanager.SportManager.RestControllers;

import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sportmanager.SportManager.Models.Event;
import com.sportmanager.SportManager.Models.EventJson;
import com.sportmanager.SportManager.Models.EventType;
import com.sportmanager.SportManager.Models.User;
import com.sportmanager.SportManager.Services.EventService;
import com.sportmanager.SportManager.Services.EventTypeService;
import com.sportmanager.SportManager.Services.MailService;
import com.sportmanager.SportManager.Services.UserService;
import com.sportmanager.SportManager.util.GenericResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/events")
@RequiredArgsConstructor
public class EventRestController {

	private final EventService eventService;
	private final EventTypeService eventTypeService;
	private final UserService userService;
	private final MailService mailService;

	@GetMapping
	public ResponseEntity<List<EventJson>> findAll() {
		List<Event> evts = eventService.findAll();
		List<EventJson> evtsJson = new ArrayList<EventJson>();
		evtsJson = evts.stream().map(element -> {
			EventJson event = new EventJson();
			event.setId(element.getId());
			event.setStartDate(element.getStartDate());
			event.setEndDate(element.getEndDate());
			event.setTypeId(element.getType().getId());
			event.setNumberOfParticipants(element.getNumberOfParticipants());
			event.setOwnerId(element.getOwner().getId());
			return event;
		}).collect(Collectors.toList());

		return ResponseEntity.ok(evtsJson);
	}

	@GetMapping(path = { "/{id}" })
	public ResponseEntity<Event> findById(@PathVariable long id) {
		return eventService.findById(id).map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

	@GetMapping(path = { "/{id}/participating" })
	public ResponseEntity<List<User>> findParticipatingUsers(@PathVariable long id) {
		return eventService.findById(id).map(record -> ResponseEntity.ok().body(record.getParticipatingUsers()))
				.orElse(ResponseEntity.notFound().build());
	}

	@PostMapping
	public ResponseEntity<Event> create(@RequestParam int numberOfParticipants, @RequestParam String startDate,
			@RequestParam String endDate, @RequestParam String currentDate, @RequestParam Long eventTypeId,
			@RequestParam Long ownerid, @RequestParam List<Long> participatingUsers) throws InterruptedException {
		Event event = new Event();
		log.info("Type id: " + String.valueOf(eventTypeId));
		log.info(String.valueOf(participatingUsers.size()));

		EventType type = eventTypeService.findById(eventTypeId).get();
		Date start = parseDate(startDate, currentDate);
		Date end = parseDate(endDate, currentDate);
		Date current = new Date();
		if (start.before(current))
			return ResponseEntity.badRequest().build();

		// Handle if event already exists in time slot
		Event result = eventIsNotConflicting(start, end, type);
		if (result != null)
			return ResponseEntity.badRequest().body(result);

		// Handle invalid number of participants
		// if(numberOfParticipants < type.getMinimumParticipants() ||
		// numberOfParticipants > type.getMaximumParticipants())
		// return ResponseEntity.badRequest().build();

		event.setNumberOfParticipants(numberOfParticipants);
		User owner = userService.findById(ownerid).get();

		event.setOwner(owner);
		event.setStartDate(start);
		event.setEndDate(end);
		event.setType(type);
		Event createdEvent = eventService.save(event);

		List<User> subscribedUsers = type.getSubscribedUsers();

		for (Long userId : participatingUsers) {
			Optional<User> userResult = userService.findById(userId);
			if (userResult.isPresent()) {
				User user = userResult.get();

				// Set event in list of participating events
				List<Event> participatingEvents = user.getParticipatingIn();
				participatingEvents.add(createdEvent);
				user.setParticipatingIn(participatingEvents);
				String messageBody = "Event starts at " + start.toString();

				// Send mail that the user has been added to the event
				if (owner.getId() != user.getId()) {
					mailService.sendMessage(user.getEmail(), "You have been added to an event", messageBody);
				}
				subscribedUsers.remove(user);

				userService.save(user);
			}
		}
		

		// Send mail to subscribed users if event is not full
		if (numberOfParticipants >= participatingUsers.size()) {
			String messageBody = "Event starts at " + start.toString();
			for (User user : subscribedUsers) {			
				mailService.sendMessage(user.getEmail(), "New event created", messageBody);
			}
		}

		return ResponseEntity.ok().body(createdEvent);
	}

	@PutMapping("/{id}")
	public GenericResponse updateEvent(@RequestBody Event event, @PathVariable long id, Authentication auth) {

		// Validate if event exists
		Optional<Event> eventOptional = eventService.findById(id);
		if (!eventOptional.isPresent())
			return new GenericResponse("Event with id=" + id + " does not exist", "error");

		User user = userService.findUserByEmail(auth.getName());

		// If user created the event continue updating
		if (eventOptional.get().getOwner().equals(user)) {
			event.setId(id);
			eventService.save(event);
			return new GenericResponse("Event successfully updated", "success");
		}

		// Else user doesn't have enough privileges
		return new GenericResponse("The logged in user does not have the privileges to edit this event", "success");
	}

	@DeleteMapping(path = { "/{id}" })
	public GenericResponse delete(@PathVariable("id") long id, Authentication auth) {

		// Validate if event exists
		Optional<Event> event = eventService.findById(id);
		if (!event.isPresent())
			return new GenericResponse("Event with id=" + id + " does not exist", "error");

		User user = userService.findUserByEmail(auth.getName());
		boolean isAdmin = AuthorityUtils.authorityListToSet(auth.getAuthorities()).contains("ADMIN");

		// If user is admin or created the event continue with deletion
		if (isAdmin || event.get().getOwner().equals(user)) {
			eventService.deleteById(id);
			return new GenericResponse("Deletion successful", "success");
		}

		// Else user doesn't have enough privileges
		return new GenericResponse("The logged in user does not have the privileges to delete this event", "error");
	}

	@PostMapping(path = "/participate")
	public GenericResponse toggleParticipating(@RequestParam(name = "userId", required = false) Integer userId,
			@RequestParam(name = "eventId", required = false) Integer eventId, Authentication auth) {

		// Validate if user and event exist
		Optional<User> userResult = userService.findById((long) userId);
		if (!userResult.isPresent())
			return new GenericResponse("User with id=" + userId + " does not exist", "error");

		Optional<Event> eventResult = eventService.findById((long) eventId);
		if (!eventResult.isPresent())
			return new GenericResponse("Event with id=" + eventId + " does not exist", "error");

		// Toggle participation
		if (userService.toggleParticipatingIn((long) userId, (long) eventId))
			return new GenericResponse("User participation in event updated", "success");

		return new GenericResponse("Something went wrong", "error");
	}

	private Date parseDate(String time, String currentDate) {
		Date date = new Date();

		String[] dates = currentDate.split(" ");
		String[] times = time.split(":");

		int month = Month.valueOf(dates[1].toUpperCase()).getValue();
		Calendar calendar = new GregorianCalendar(Integer.parseInt(dates[2]), month == 1 ? 12 : month - 1,
				Integer.parseInt(dates[0]), Integer.parseInt(times[0]), Integer.parseInt(times[1]),
				Integer.parseInt(times[2]));

		return calendar.getTime();
	}

	private Event eventIsNotConflicting(Date start, Date end, EventType eventType) {
		for (Event event : eventType.getEvents()) {
			if ((start.after(event.getStartDate()) && start.before(event.getEndDate()))
					|| (end.after(event.getStartDate()) && end.before(event.getEndDate()))
					|| (start.compareTo(event.getStartDate()) == 0)) {
				return event;
			}
		}
		return null;
	}
}
