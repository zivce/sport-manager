package com.sportmanager.SportManager.Models;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String email;

	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;

	private String location;

	private String name;
	private int avatarId;
	private boolean confirmed;

	@JsonManagedReference
	@OneToMany(targetEntity = Event.class, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "owner")
	private List<Event> createdEvents;

	@ManyToMany(targetEntity = Event.class, fetch = FetchType.LAZY)
	@JoinTable(name = "PARTICIPATING_IN", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "event_id"))
	private List<Event> participatingIn;

	@ManyToMany(targetEntity = EventType.class, fetch = FetchType.LAZY)
	@JoinTable(name = "SUBSCRIBED_TO", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "eventtype_id"))
	private List<EventType> subscribedTo;

	@ManyToMany(targetEntity = Role.class, fetch = FetchType.LAZY)
	@JoinTable(name = "USER_ROLE", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;

	@CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
	private Date updatedAt;

	public String highestRole() {

		String highestRole = "";
		long i = 1;
		for (Role role : this.roles) {
			if (role.getId() >= i)
				highestRole = role.getRole();
			i++;
		}

		return highestRole;
	}

	public boolean isAdmin() {
		if (this.highestRole().compareToIgnoreCase("ADMIN") == 0)
			return true;
		else
			return false;
	}
}
