package com.sportmanager.SportManager.Models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "eventtypes")
public class EventType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	private int minimumParticipants;

	private int maximumParticipants;

	private int maxDuration;

	@JsonBackReference
	@OneToMany(targetEntity = Event.class, orphanRemoval = true, mappedBy = "type")
	private List<Event> events;

	@JsonBackReference
	@ManyToMany(targetEntity = User.class, mappedBy = "subscribedTo")
	private List<User> subscribedUsers;

	@CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
	private Date updatedAt;

	@PreRemove
	private void removeSubscribedUsers() {
		for (User u : subscribedUsers) {
			u.getSubscribedTo().remove(this);
		}
	}

}
