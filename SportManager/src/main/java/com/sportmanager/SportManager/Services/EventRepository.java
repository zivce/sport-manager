package com.sportmanager.SportManager.Services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sportmanager.SportManager.Models.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

}
