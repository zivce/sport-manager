package com.sportmanager.SportManager.Services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.sportmanager.SportManager.Models.Event;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class EventService {

	private final EventRepository eventRepository;

	public List<Event> findAll() {
		return eventRepository.findAll();
	}

	public Optional<Event> findById(Long id) {
		return eventRepository.findById(id);
	}

	public Event save(Event event) {
		return eventRepository.save(event);
	}

	public void deleteById(Long id) {
		eventRepository.deleteById(id);
	}
}
