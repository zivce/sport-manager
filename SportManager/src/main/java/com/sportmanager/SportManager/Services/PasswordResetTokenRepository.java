package com.sportmanager.SportManager.Services;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sportmanager.SportManager.Models.PasswordResetToken;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {

	PasswordResetToken findByToken(String token);
}
