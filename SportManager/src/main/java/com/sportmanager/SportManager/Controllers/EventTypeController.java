package com.sportmanager.SportManager.Controllers;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.sportmanager.SportManager.Models.EventType;
import com.sportmanager.SportManager.Models.User;
import com.sportmanager.SportManager.Services.EventTypeService;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class EventTypeController {

	private final EventTypeService eventTypeService;

	@GetMapping("/admin/create-event-type")
	public String createForm() {
		return "create-event-type";
	}

	@PostMapping("/admin/create-event-type")
	public String createEventType(@RequestParam(value = "nameO", required = true) String name,
			@RequestParam(value = "minPartO", required = true) Integer minPart,
			@RequestParam(value = "maxPartO", required = true) Integer maxPart,
			@RequestParam(value = "maxDurationO", required = true) Integer maxDuration, @ModelAttribute EventType e,
			Model m) {

		if (name.equals("") || minPart < 1 || minPart == null || maxPart > 300 || maxPart < 1 || maxPart == null
				|| maxPart < minPart || maxDuration == null || maxDuration < 1)

		{
			m.addAttribute("name", name);
			m.addAttribute("minPart", minPart);
			m.addAttribute("maxPart", maxPart);
			m.addAttribute("maxDuration", maxDuration);
			return "create-event-type";

		} else {
			EventType temp = new EventType();
			temp.setName(name);
			temp.setMinimumParticipants(minPart);
			temp.setMaximumParticipants(maxPart);
			temp.setMaxDuration(maxDuration);

			eventTypeService.save(temp);

			return "event-type-created";
		}
	}

	@GetMapping("/admin/edit-event-type")
	public String editingForm(@RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "minPart", required = true) int minPart,
			@RequestParam(value = "maxPart", required = true) int maxPart,
			@RequestParam(value = "maxDuration", required = true) int maxDuration, Model model) {
		return "create-event-type";
	}

	@PostMapping("/admin/edit-event-type")
	public String editEventType(@RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "nameO", required = true) String name,
			@RequestParam(value = "minPartO", required = true) Integer minPart,
			@RequestParam(value = "maxPartO", required = true) Integer maxPart,
			@RequestParam(value = "maxDurationO", required = true) Integer maxDuration, Model m) {

		if (id == null || name.equals("") || minPart < 1 || minPart == null || maxPart > 300 || maxPart < 1
				|| maxPart == null || maxPart < minPart || maxDuration == null || maxDuration < 1) {

			if (id == null) {
				m.addAttribute("error", "ID does not exist!");
				return "event-type-error";
			} else {
				m.addAttribute("param.id", id);
				m.addAttribute("name", name);
				m.addAttribute("minPart", minPart);
				m.addAttribute("maxPart", maxPart);
				m.addAttribute("maxDuration", maxDuration);
				return "create-event-type";
			}
		}

		else {
			Optional<EventType> temp = eventTypeService.findById(id);
			EventType tempS;

			if (temp.isPresent()) {
				tempS = temp.get();
				tempS.setName(name);
				tempS.setMinimumParticipants(minPart);
				tempS.setMaximumParticipants(maxPart);
				tempS.setMaxDuration(maxDuration);

				eventTypeService.save(tempS);
				return "event-type-created";
			} else {

				m.addAttribute("error", "Event Type does not exist in DB");
				return "event-type-error";
			}

		}

	}

	@ExceptionHandler(Exception.class)
	public String handleError(HttpServletRequest req, Exception ex, Model m) {
		m.addAttribute("error", ex);
		return "event-type-error";
	}

	@GetMapping("/admin/event-type-list")
	public String getEventTypeList(Model model, @RequestParam(value = "p", required = false) Integer page) {

		if (page == null)
			page = 1;

		int resultsPerPage = 10;
		int totalNumberOfResults = eventTypeService.countEventTypes();
		int numberOfPages = totalNumberOfResults / resultsPerPage + 1;

		List<EventType> eventTypeList = eventTypeService.findByPage(page, resultsPerPage);
		final String eventTypeListAttribute = "eventTypeList";

		model.addAttribute(eventTypeListAttribute, eventTypeList);
		model.addAttribute("numberOfPages", numberOfPages);
		model.addAttribute("totalNumberOfResults", totalNumberOfResults);
		model.addAttribute("resultsPerPage", resultsPerPage);
		model.addAttribute("currentPage", page);

		return "event-type-list";
	}

	@PostMapping("/admin/delete-event-type")
	public RedirectView deleteEventType(@RequestParam(required = true) long id) {
		eventTypeService.deleteById(id);

		final String eventTypeListUrl = "/admin/event-type-list";
		return new RedirectView(eventTypeListUrl);
	}

}
