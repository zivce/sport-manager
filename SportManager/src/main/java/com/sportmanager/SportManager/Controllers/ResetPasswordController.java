package com.sportmanager.SportManager.Controllers;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sportmanager.SportManager.Models.User;
import com.sportmanager.SportManager.Services.MailService;
import com.sportmanager.SportManager.Services.UserService;
import com.sportmanager.SportManager.util.GenericResponse;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class ResetPasswordController {

	private final UserService userService;
	private final MailService mailService;
	private final MessageSource messages;

	@GetMapping(value = "/forgotPassword")
	public String greetingForm() {
		return "forgotPassword";
	}

	@GetMapping(value = "/resetPassword")
	public String resetPassword(@RequestParam("id") long id, @RequestParam("token") String token, Model model) {
		String result = userService.validatePasswordResetToken(id, token);

		if (result.equals("confirmed")) {
			User user = userService.findById(id).get();
			model.addAttribute("user", user);
		}
		model.addAttribute("result", result);
		return "updatePassword";
	}

	@PostMapping(value = "/resetPassword")
	public String resetPasswordConfirmed(@RequestParam("id") long id, @RequestParam("password") String password) {

		User user = userService.findById(id).get();
		user.setPassword(password);
		userService.save(user);
		return "login";
	}

	@GetMapping(value = "/messagePassword")
	public String resetPassword() {
		return "message";
	}

	@RequestMapping(value = "/user/resetPassword", method = RequestMethod.POST)
	@ResponseBody
	public GenericResponse resetPassword(HttpServletRequest request, @RequestParam("email") String userEmail) throws InterruptedException {

		User user = userService.findUserByEmail(userEmail);
		if (user != null) {
			final String token = UUID.randomUUID().toString();
			userService.createPasswordResetTokenForUser(user, token);

			final String contextPath = "http://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath();
			final String url = contextPath + "/resetPassword?id=" + user.getId() + "&token=" + token;
			final String message = "We heard that you lost your EventManager password. Sorry about that!\nBut don’t worry! You can use the following link to reset your password:\n";
			final String message2 = "\n\nIf you don’t use this link within 3 hours, it will expire.\n\nThanks,\nYour friends at EventManager";
			String body = message + " \r\n" + url + message2;

			mailService.sendMessage(user.getEmail(), "Please Reset Your Password", body);
		}
		return new GenericResponse(messages.getMessage("message.resetPasswordEmail", null, request.getLocale()));
	}

}
