package Validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import DTOs.RegisterUserDto;

public class PasswordMatchesValidator
	implements ConstraintValidator<PasswordMatches, Object> { 
	     
	    @Override
	    public void initialize(PasswordMatches constraintAnnotation) {       
	    }
	    @Override
	    public boolean isValid(Object obj, ConstraintValidatorContext context){   
	        RegisterUserDto user = (RegisterUserDto) obj;
	        return user.getPassword().equals(user.getConfirmPassword());    
	    }     
}
