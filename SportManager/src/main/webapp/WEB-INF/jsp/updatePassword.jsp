<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link href="/admin_root/dist/css/background.css" rel="stylesheet" type="text/css">
<link href="/admin_root/dist/css/updatePassword.css" rel="stylesheet" type="text/css">
<title>Change Password</title>
</head>
<body>
	<div class="main">

	<c:set var = "res" scope = "session" value = "${result}"/>
	
	<div class="container">
		
		<!-- INVALID -->
		<c:if test = "${res == 'invalid'}">		
		<div class="row">
			<div class="col-sm-12">
				<h1>Password reset token invalid</h1>
			</div>
		</div>
		</c:if>	
		
		<!-- EXPIRED -->
		<c:if test = "${res == 'expired'}">	
		<div class="row">
			<div class="col-sm-12">
				<h1>Password reset token expired</h1>
			</div>
		</div>
		</c:if>	
		
		<!-- CONFIRMED -->			
		<c:if test = "${res == 'confirmed'}">	
		<div class="row">
			
				<h1 class="text-center form-heading">Change Password</h1>
				<p class="text-center form-text">Use the form below to change your password. Your password cannot be the same as your username.</p>
				<form method="post" id="passwordForm" action="/resetPassword">
					<input type="password" class=" form-control" name="password" id="password" placeholder="New Password" autocomplete="off">
					
					<input type="password" class=" form-control" name="passwordConfirm" id="passwordConfirm" placeholder="Repeat Password" autocomplete="off">					
					<input type="hidden" id="id" name="id" value=${user.id}>
					<input type="submit" class="btn btn-block  btn-load btn-lg" data-loading-text="Changing Password..." value="Change Password">
				</form>
			
		</div>
		</c:if>	
		</div>
	</div>
</body>
</html>