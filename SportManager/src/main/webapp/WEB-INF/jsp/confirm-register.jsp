<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Register information</title>	
	<link href="/css/error-page.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="content">		
	
		<div>
			
		</div>
		<c:choose>
		
		<c:when test = "${registerSuccess}">
			<div>
				<p class="status"><span class="status__message">Registration completed successfully</span></p>	
				<p class="status"><span class="status__message"><a href="/login">Click here</a> to login</span></p>
			</div>
		</c:when>
		<c:otherwise>
			<p class="status"><span class="status__message">Registration not successfull. Broken or invalid token.</span></p>			
		</c:otherwise>
		
		</c:choose>



		
	</div>
</body>
</html>