<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title> Create event type </title>
    
    <link href="../webjars/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet" type="text/css">	
    <!-- <link href="css/event-type-form.css" rel="stylesheet" type="text/css"> -->
    <!-- <link href="/css/navigation.css" rel="stylesheet" type="text/css"> -->
    <link href="/admin_root/dist/css/event-type-form.css" rel="stylesheet" type="text/css">
    <link href="/admin_root/dist/css/navigation.css" rel="stylesheet" type="text/css">
    <link href="/admin_root/dist/css/create-event-type.css" rel="stylesheet" type="text/css">
    
	
    
   
    
    <!--TO DO-->
</head>
<body>
	<%@ include file="common/navigation.jspf" %>
	<div class="main">
	<div class="create-form">
	<div class="container" >
		<form id="eventTypeForm" class="form-signin needs-validation" method="POST">
			<h2 class="form-heading"><c:choose>
			    <c:when test="${param.id != null || id!=null}">
			    
			         Edit Event Type
			    </c:when>
			    
			    <c:otherwise>
			         Create Event Type
			    </c:otherwise>
			    </c:choose>
			</h2>
			
				<c:if test = "${param.id != null}">
         			<input type="hidden" name="id" value="${param.id}">
      				</c:if>
      				
      				<c:if test = "${id != null}">
         			<input type="hidden" name="id" value="${id}">
      				</c:if>
      				
			<div class="form-group">
			
        		<label for="name">Name of an Event type:</label>
        			<input placeholder="e.g. Table Tennis"  class="form-control" type="text" id="name"name="nameO"
        				<c:if test = "${name != null}">
         			    value= "${name}"
                      </c:if>
                      <c:if test = "${param.name != null}">
         			    value="${param.name}"
                      </c:if>  autofocus required>
				<span class="help-block"></span>
    		</div>
    		
    		<div class="form-group">
			
        		<label for="minPart">Minimum number of participants:</label><input type="number"  class="form-control" min="1" id="minPart" name="minPartO" 
        			 <c:if test = "${minPart != null}">
                        value="${minPart}"
                      </c:if>
                      <c:if test = "${param.minPart != null}">
                        value="${param.minPart}"
                      </c:if>  required> 
				<span class="help-block"></span>
    		</div>
    		
    		<div class="form-group">
			
        		<label for="maxPart">Maximum number of participants:</label><input type="number" class="form-control"  min="1" max="300" id="maxPart" name="maxPartO" 
        			<c:if test = "${maxPart != null}">
                        value="${maxPart}"
                      </c:if>
                      <c:if test = "${param.maxPart != null}">
                        value="${param.maxPart}"
                      </c:if> 
                      required>
                      <span class="help-block"></span>
    		</div>
				
    		<div class="form-group">
        		<label for="maxDuration">Maximum possible duration:</label>
						<input type="number" class="form-control" min="1" max="200" name="maxDurationO" id="maxDuration"
        						<c:if test = "${maxDuration != null}">
                        value="${maxDuration}"
                    </c:if>
                    <c:if test = "${param.maxDuration != null}">
                        value= "${param.maxDuration}"
                    </c:if>  required>
                    <span class="help-block"></span>
    		</div>
			<button class="btn btn-lg btn-primary btn-block">Submit</button>
			<input
								type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
		</form>
	</div>
	<script src="/admin_root/dist/js/event-type-validation.js" defer></script>
	<script src="/admin_root/dist/js/navigation.js"></script>
</body>
