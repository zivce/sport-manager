<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title> Cart </title>
    <link rel="stylesheet" href="./styles/designCET.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>
<body>
   
   	<h1>Operation failed!</h1>
    <h3>Error! <c:if test = "${param.error!=null}">
    													<c:out value="${param.error}"/>
    												</c:if>
    												<c:if test = "${error!=null}">
    													<c:out value="${error}"/>
    												</c:if></h3>
    												<h2>Please enter the correct arguments!</h2>
    												
    												
    												
    												<script>
  	setTimeout(function() {
      document.location = "/admin/event-type-list";
 		 }, 2500); // <-- this is the delay in milliseconds
	</script> 
</body>