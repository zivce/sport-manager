<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<meta charset="UTF-8">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="/admin_root/dist/css/background.css" rel="stylesheet" type="text/css">
<link href="/admin_root/css/forgotPassword.css" rel="stylesheet" type="text/css">
<title>Reset Password</title>
</head>
<body>
	
	
	<div class="main">
	<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 pass">
            <div class="panel panel-default">
	<div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <h2 class="text-center">Forgot Password?</h2>
                  <p>You can reset your password here.</p>
                  <div class="panel-body">
                    <input id="email" name="email" placeholder="email address" class="form-control"  type="email">
                    <button type="submit" onclick="resetPass()" class="btn btn-lg  btn-block">Send password reset email</button>
                  </div>
                </div>
              </div></div></div></div></div></div>


	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

	<script>
		function resetPass() {
			var email = $("#email").val();

			$.post(
							"/user/resetPassword",
							{
								email : email
							},
							function(data) {
								window.location.href = "/messagePassword";
							})
					.fail(
							function(data) {
								if (data.responseJSON.error
										.indexOf("MailError") > -1) {
									window.location.href = "/emailError.html";
								} else {
									window.location.href = "/login?message="
											+ data.responseJSON.message;
								}
							});
		}
	</script>

</body>
</html>