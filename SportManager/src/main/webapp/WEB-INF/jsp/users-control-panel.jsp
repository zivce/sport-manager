<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
	<meta charset="ISO-8859-1">
	<link href="../webjars/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/admin_root/dist/css/table-pagination-styles.css" rel="stylesheet" type="text/css">
	<script> 
	function acceptDelete() {
		return confirm("Are you sure you want to delete user?");
	}

	function acceptUpgrade() {
		return confirm("Are you sure you want to upgrade user?");
	}

	function acceptDowngrade() {
		return confirm("Are you sure you want to downgrade user?");
	}
	</script>

<title>Users control panel</title>
	<link href="/admin_root/dist/css/navigation.css" rel="stylesheet" type="text/css">
	<title>Users control panel</title>
</head>

<body>
	<%@ include file="common/navigation.jspf" %>
	<div class="main">
	<c:set var="baseUrl" value="users-control-panel" />
	<div class="create-form">
	<div class="container">
		<div class="table_title">
			<div class="row">
				<div class="col-sm-5">
					<h2>User Management</h2>
				</div>
			</div>
		</div>
		<div class="table_wrapper">

			<table class="table">
				<thead>
					<tr class="table_header">
						<th class="col-md-2">Name</th>
						<th class="col-md-3">E-mail</th>
						<th class="col-md-2">Location</th>
						<th class="col-md-2">Role</th>
						<th class="col-md-1">Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${usersList}" var="user">

						<tr class="table_row">
							<td>${user.getName()}</td>
							<td>${user.getEmail()}</td>
							<td>${user.getLocation()}</td>
							<td>${user.highestRole()}</td>
							<td class="table_actions">

								<c:if test="${!loggedInUserEmail.equals(user.getEmail())}">

								<c:choose>
								<c:when test="${!user.isAdmin()}">
										<form class="action_item" action="${baseUrl}/upgrade" method="POST"
											onsubmit="return acceptUpgrade()">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<input type=hidden name=upgradeUserId value="${user.getId()}">
											<button type="submit" name="upgradeUserSubmit"
												class="actions_btn btn_upgrade" title="Upgrade user"></button>
										</form>
								</c:when>	
								<c:otherwise>
										<form class="action_item" action="${baseUrl}/downgrade" method="POST"
											onsubmit="return acceptDowngrade()">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<input type=hidden name=downgradeUserId value="${user.getId()}">
											<button type="submit" name="downgradeUserSubmit"
												class="actions_btn btn_downgrade" title="Downgrade user"></button>
										</form>
								</c:otherwise>		
								</c:choose>

									
										<form class="action_item" action="${baseUrl}/delete" method="POST"
											onsubmit="return acceptDelete()">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<input type=hidden name=deleteUserId value="${user.getId()}">
											<button type="submit" name="deleteUserSubmit" class="actions_btn btn_delete"
												title="Delete user"></button>
										</form>
								
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</div>
		<div class="pagination_wrapper">
			<ul class="pagination">
				<li class="page-item prev-next"><a href="?p=${currentPage==1 ? 1 : currentPage-1 }" >Previous</a></li>
				<c:forEach var="i" begin="1" end="${numberOfPages}">
					<li class="page-item ${currentPage==i ? 'active' : '' }"><a href="?p=${i}"
							class="page-link">${i}</a></li>
				</c:forEach>
				<li class="page-item prev-next"><a href="?p=${currentPage==numberOfPages ? numberOfPages : currentPage+1 }"
						class="page-link">Next</a></li>
			</ul>
			<div class="hint-text"><b>${totalNumberOfResults}</b> total users</div>
		</div>
	</div></div>
	</div>
	<script src="/admin_root/dist/js/navigation.js"></script>
</body>

</html>