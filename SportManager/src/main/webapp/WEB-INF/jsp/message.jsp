<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="/admin_root/dist/css/background.css" rel="stylesheet" type="text/css">
<title>Insert title here</title>
</head>
<body>
<div class="main">
<div class="container">
<h2 class="mail-text">Check your email for a link to reset your password. </br>If it does not appear within a few minutes, check your spam folder.</h2>
</div></div>
</body>
</html>