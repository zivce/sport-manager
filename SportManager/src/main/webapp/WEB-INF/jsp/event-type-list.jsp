<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Event Type List</title>
<link href="../webjars/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/admin_root/dist/css/table-pagination-styles.css" rel="stylesheet" type="text/css">
<link href="/admin_root/dist/css/navigation.css" rel="stylesheet" type="text/css">

<script src="/admin_root/dist/js/event-type-list.js" defer></script>

<script>
function confirmEventTypeDelete() {
	return confirm("Are you sure you want to delete?");	
}
</script>
</head>


<body>
	
	<%@ include file="common/navigation.jspf" %>
	<div class="main">
	<div class="create-form">
	<div class="container">
	
		<div class="table_title">
			<div class="row">
				<div class="col-sm-5">
					<h2>Event types</h2>
				</div>
			</div>
		</div>
		<div class="table_wrapper">
			<table class="table">
				<thead>
					<tr class="table_header">
						<th>Name</th>
						<th>Min. Participants</th>
						<th>Max. Participants</th>
						<th>Max. Duration</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody class="table_row">
					<c:forEach items="${eventTypeList}" var="eventType">
						<c:set var="eventTypeEditUrl" value="/edit-event-type?
				id=${eventType.id}
				&name=${eventType.name}
				&minPart=${eventType.minimumParticipants}
				&maxPart=${eventType.maximumParticipants}
				&maxDuration=${eventType.maxDuration}" />

						<c:set var="eventTypeDeleteUrl" 
							value="/admin/delete-event-type" />

						<tr>
							<td>${eventType.name}</td>
							<td>${eventType.minimumParticipants}</td>
							<td>${eventType.maximumParticipants}</td>
							<td>${eventType.maxDuration}</td>
							<td class="table_actions">
								<div class=action_item>
									<a class="actions_btn btn_edit" title="Edit event type"
										href="${eventTypeEditUrl}"></a>
								</div>
								<form class="action_item" action="${eventTypeDeleteUrl}" method="post"
									onsubmit="return confirmEventTypeDelete()">
									<input name="id" value="${eventType.id}" type="hidden" />
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									<button type="submit" class="actions_btn btn_delete"
										title="Delete event type"></button>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>


			</table>

		</div>

		<div class="pagination_wrapper">
			<ul class="pagination">
				<li class="page-item"><a href="?p=${currentPage==1 ? 1 : currentPage-1 }">Previous</a></li>
				<c:forEach var="i" begin="1" end="${numberOfPages}">
					<li class="page-item ${currentPage==i ? 'active' : '' }"><a href="?p=${i}"
							class="page-link">${i}</a></li>
				</c:forEach>
				<li class="page-item"><a href="?p=${currentPage==numberOfPages ? numberOfPages : currentPage+1 }"
						class="page-link">Next</a></li>
			</ul>

			<div class="hint-text"><b>${totalNumberOfResults}</b> total event types</div>
		</div>
	</div></div>
	</div>
<script src="/admin_root/dist/js/navigation.js"></script>

</body>

</html>