const gulp = require('gulp');
const browserify = require("browserify");
const source = require('vinyl-source-stream');
const tsify = require("tsify");
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const cleanCSS = require('gulp-clean-css');
const del = require('del');
const merge = require('merge-stream');
const glob = require('glob');

const paths = {
    srcCodeDir: './src/',
    distDir: './dist/',
    tomcatAppSourceFolder: "../SportManager/SportManagerClient/dist/"
};

const regexPatterns = {
    scssMatcher: paths.srcCodeDir + '**/*.scss',
    tsMatcher: paths.srcCodeDir + '**/*.ts',
    htmlMatcher: paths.srcCodeDir + '**/*.html',
    avatarMatcher: paths.srcCodeDir + "avatar/*",
    iconsMatcher: paths.srcCodeDir + "icons/*",
    imagesMatcher: paths.srcCodeDir + "images/*",
    distDirMatcher: './dist/**'
}

const compileTypescriptTask = function () {
    const entries = glob.sync(regexPatterns.tsMatcher);
    return merge(entries.map(entry => browserify({
        basedir: './',
        debug: true,
        entries: entry,
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .bundle()
        .pipe(source('js/' + entry.slice(0, -3).slice(9) + '.js'))
        .pipe(gulp.dest(paths.distDir))));

};

gulp.task('compile-typescript', compileTypescriptTask);

gulp.task('compile-typescript:watch', function () {
    gulp.watch(regexPatterns.tsMatcher, compileTypescriptTask);
});

const sassTask = function () {
    return gulp.src(regexPatterns.scssMatcher)
        .pipe(sass({
            includePaths: ['node_modules']
        }).on('error', sass.logError))
        .pipe(cleanCSS({ inline: ['local'] }))
        .pipe(gulp.dest(paths.distDir));
}

gulp.task('sass', sassTask);

gulp.task('sass:watch', function () {
    gulp.watch(regexPatterns.scssMatcher, sassTask);
});

const cleanBuildDir = function () {
    return del([regexPatterns.distDirMatcher]);
}
gulp.task('build:clean', cleanBuildDir);

const copyHtmlToPublicTask = function () {
    return gulp.src('./src/components/*.html')
        .pipe(gulp.dest('./dist/'));
}

gulp.task('copy-html-to-public', copyHtmlToPublicTask)
gulp.task('copy-html-to-public:watch', function () {
    gulp.watch(regexPatterns.htmlMatcher, copyHtmlToPublicTask)
})

const copyDistToTomcat = function () {
    return gulp.src(regexPatterns.distDirMatcher)
        .pipe(gulp.dest(paths.tomcatAppSourceFolder));
}

gulp.task('copy-dist-to-tomcat', copyDistToTomcat)
gulp.task('copy-dist-to-tomcat:watch', function () {
    gulp.watch(regexPatterns.distDirMatcher, copyDistToTomcat)
})

const cleanDistDir = function () {
    return del([paths.tomcatAppSourceFolder], { force: true });
}
gulp.task('tomcat-dist:clean', cleanDistDir);

const copyAvatars = function(){
    return gulp.src('./src/avatar/*')
    .pipe(gulp.dest('./dist/avatar/'));
}

gulp.task('copy-avatar-to-dist', copyAvatars);
gulp.task('copy-avatar-to-dist:watch', function(){
    gulp.watch(regexPatterns.avatarMatcher, copyAvatars)
});

const copyImages = function(){
    return gulp.src('./src/images/*')
    .pipe(gulp.dest('./dist/images/'));
}

gulp.task('copy-images-to-dist', copyImages);
gulp.task('copy-images-to-dist:watch', function(){
    gulp.watch(regexPatterns.imagesMatcher, copyImages)
});

const copyIcons = function(){
    return gulp.src('./src/icons/*')
    .pipe(gulp.dest('./dist/icons/'));
}

gulp.task('copy-icons-to-dist', copyIcons);
gulp.task('copy-icons-to-dist:watch', function(){
    gulp.watch(regexPatterns.iconsMatcher, copyIcons)
});

gulp.task('default',
        gulp.series(
            gulp.parallel('compile-typescript', 'sass', 'copy-html-to-public', 
            'copy-avatar-to-dist', 'copy-icons-to-dist', 'copy-images-to-dist'),
            gulp.series('tomcat-dist:clean',
                gulp.parallel('copy-dist-to-tomcat',
                    'copy-dist-to-tomcat:watch',
                    'compile-typescript:watch',
                    'sass:watch',
                    'copy-html-to-public:watch',
                    'copy-images-to-dist:watch')),
        )); 