import apiJSON from "./api";

var tbl = document.getElementsByTagName("table");
var eventTypes;
var mySubscriptions = [];
// @ts-ignore
// var myId: number = window.client.id;
var passwordField;
var passwordConfirmField;
var selectedAvatar: String;
var btn;
var avatarsFetched: boolean = false;
var accountDetails;

window.addEventListener('load', function () {
  getUserData();
  passwordField = <HTMLInputElement>document.getElementById("password");
  passwordConfirmField = <HTMLInputElement>document.getElementById("passwordConfirm");
  btn = <HTMLButtonElement>document.getElementById("changePasswordButton");
  // @ts-ignore
  $("#changePasswordButton").click(function (e) {
    e.preventDefault();
    validatePassword();
  });
  // @ts-ignore
  $("#changeAvatarButton").click(function (e) {
    e.preventDefault();
    changeAvatar();
  });
  // @ts-ignore
  $("#changeCityButton").click(function (e) {
    e.preventDefault();
    changeCity();
  });
  // @ts-ignore
  $('#collapseThree').on('show.bs.collapse', function () {
    if (!avatarsFetched) {
      var avatarList = <HTMLUListElement>document.getElementById("avatarSelection");
      //directory count ?
      for (let i = 1; i <= 30; i++) {
        let avatar = <HTMLLIElement>document.createElement("li");
        let img = document.createElement("img");
        img.setAttribute("id", "avatar" + i.toString());
        img.setAttribute("src", "./avatar/" + i + ".svg");
        img.setAttribute("width", "82px");
        img.setAttribute("height", "auto");
        img.setAttribute("data-avatar-id", i.toString());
        avatar.appendChild(img);
        avatarList.appendChild(avatar);


        img.onclick = function () {
          for (let i = 1; i <= 30; i++) {
            document.getElementById("avatar" + i).setAttribute("style", "border:none;");
            document.getElementById("avatar" + i).setAttribute("style", "");
          }
          (<HTMLImageElement>this).setAttribute("style", "border: 4px solid #428BCA;")
          selectedAvatar = (<HTMLImageElement>this).getAttribute("data-avatar-id");
        }
      }

      document.getElementById("avatar" + accountDetails.avatarId.toString())
        .setAttribute("style", "border: 4px solid #428BCA;");

      avatarsFetched = true;
    }
  });
  // @ts-ignore
  $('#collapseFour').on('show.bs.collapse', function () {
    var selectCity = <HTMLSelectElement>document.getElementById("selectCity");
    selectCity.value = accountDetails.location;
  });

});

function createNotification(parent, message, type) {
  var notification = document.createElement("div");
  notification.className = "form-group alert alert-" + type;
  notification.setAttribute("style", " float:left;");
  notification.innerHTML += message;
  parent.appendChild(notification);
  setTimeout(function () {
    parent.removeChild(notification);
  }, 3000);
}
function changeCity() {
  var selectCity = <HTMLSelectElement>document.getElementById("selectCity");
  var selectedCity = selectCity.options[selectCity.selectedIndex].value;
  if (selectedCity == accountDetails.location) {
    alert("This is already your city");
  }
  else if (selectedCity == "Nis" || selectedCity == "Belgrade" || selectedCity == "Kragujevac") {
    var data = {}
    data["city"] = selectedCity;
    //@ts-ignore
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: apiJSON.CHANGE_CITY + accountDetails.id,
      data: JSON.stringify(data),
      dataType: 'json',
      cache: false,
      timeout: 600000,
      success: function (data) {
        if (data == true) {
          createNotification(document.getElementById("changeCityButton").parentNode, "Your location has been succesfully changed", "success");
          accountDetails.location = selectedCity;
        }
        if (data == false) {
          createNotification(document.getElementById("changeCityButton").parentNode, "Invalid selection for city", "danger");
        }
      },
      error: function (e) {
        createNotification(document.getElementById("changeCityButton").parentNode, "There has been an issue while trying to change the city. Please try again.", "danger");
      }
    });
  }
  else {
    createNotification(document.getElementById("changeCityButton").parentNode, "Invalid selection for city", "danger");
  }
}
function changeAvatar() {

  if (accountDetails.avatarId !== parseInt(selectedAvatar.toString())) {
    var data = {}
    data["avatarId"] = selectedAvatar;
    //@ts-ignore
    $.ajax({
      type: "POST",
      contentType: "application/json",
      url: apiJSON.AVATAR_URL + accountDetails.id,
      data: JSON.stringify(data),
      dataType: 'json',
      cache: false,
      timeout: 600000,
      success: function (data) {
        if (data == true) {
          createNotification(document.getElementById("changeAvatarButton").parentNode, "You have succesfully changed your avatar.", "success");
          accountDetails.avatarId = selectedAvatar;
          passwordField.value = "";
          passwordConfirmField.value = "";
        }
        if (data == false) {
          createNotification(document.getElementById("changeAvatarButton").parentNode, "Invalid selection for avatar", "danger");

        }
      },
      error: function (e) {

        createNotification(document.getElementById("changeAvatarButton").parentNode, "There has been an issue while trying to change your avatar. Please try again.", "danger");

      }
    });
  }
  else {
    createNotification(document.getElementById("changeAvatarButton").parentNode, "This is already your avatar.", "secondary");
  }
}
function getUserData() {

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      accountDetails = JSON.parse(this.responseText);
      loadSubscriptions();
      (<any>document.getElementById("avatarimg")).setAttribute("src", "./avatar/" + accountDetails.avatarId + ".svg");
      (<any>document.getElementById("avatarimg")).setAttribute("title", accountDetails.name);
      (<any>document.getElementById("avatarimg")).setAttribute("data-toggle", "tooltip");
      (<any>document.getElementById("avatarimg")).setAttribute("data-placement", "left");
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      });
    }

  };

  fetch('http://localhost:8080/api/users/current')
    .then(function (response) {
      return response.json();
    })
    .then(function (user) {
      xhttp.open("GET", "http://localhost:8080/api/users/" + user.id, true);
      xhttp.send();
    })

}
function toggle(value) {

  var data = {}
  data["eventTypeId"] = value;

  // @ts-ignore
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: apiJSON.SUBSCRIPTIONS_URL + accountDetails.id,
    data: JSON.stringify(data),
    dataType: 'json',
    cache: false,
    timeout: 600000,
    success: function (data) {


    },
    error: function (e) {
      alert("Subscribing/Unsubscribing has failed! Please try again.");
      // @ts-ignore
      $(function () {
        // @ts-ignore
        $('#' + value).bootstrapToggle('toggle');
      });
    }
  });

}
function changePassword() {

  var data = {}
  data["password"] = passwordField.value;
  // @ts-ignore
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: apiJSON.CHANGE_PASSWORD_URL + accountDetails.id,
    data: JSON.stringify(data),
    dataType: 'json',
    cache: false,
    timeout: 600000,
    success: function (data) {
      if (data == 0) {

        createNotification(document.getElementById("changePasswordButton").parentNode, "You have succesfully changed your password.", "success");
        passwordField.value = "";
        passwordConfirmField.value = "";
      }
    },
    error: function (e) {

      createNotification(document.getElementById("changePasswordButton").parentNode, "There has been an issue while trying to change your password. Please try again.", "danger");

    }
  });

}

function loadEventTypes() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      eventTypes = JSON.parse(this.responseText);

      fillTable();
    }
  };
  xhttp.open("GET", apiJSON.EVENT_TYPES_URL, true);
  xhttp.send();
}

function loadSubscriptions() {

  var a = accountDetails.subscribedTo.length;
  for (let i = 0; i < a; i++)
    mySubscriptions.push(accountDetails.subscribedTo[i].id)
  loadEventTypes();

}

function fillTable() {
  var row: HTMLTableRowElement;
  var cell: HTMLTableCellElement;

  eventTypes.forEach(function (value) {

    row = document.createElement("tr");
    cell = document.createElement("td");
    cell.innerHTML = value.name;
    row.appendChild(cell);
    cell = document.createElement("td");
    if (mySubscriptions.includes(value.id)) {
      let x = document.createElement("input");
      x.setAttribute("type", "checkbox");
      x.setAttribute("id", value.id);
      x.setAttribute("data-width", "140");
      x.setAttribute("data-on", "Subscribed");
      x.setAttribute("data-off", "Unsubscribed");
      x.addEventListener("change", function () { toggle(value.id) });
      cell.appendChild(x);
      // @ts-ignore
      $(function () {
        // @ts-ignore
        $('#' + value.id).bootstrapToggle('on');
      });
    }
    else {
      let x = document.createElement("input");
      x.setAttribute("type", "checkbox");
      x.setAttribute("id", value.id);
      x.setAttribute("data-width", "140");
      x.setAttribute("data-on", "Subscribed");
      x.setAttribute("data-off", "Unsubscribed");
      x.addEventListener("change", function () { toggle(value.id) });
      cell.appendChild(x);
      // @ts-ignore
      $(function () {
        // @ts-ignore
        $('#' + value.id).bootstrapToggle('off');
      });

    }

    //@ts-ignore
    $(function () {
      //@ts-ignore
      $('#' + value.id).change(function () {
        toggle(value.id);
      })
    })

    row.appendChild(cell);
    tbl[0].appendChild(row);
  });
}

function validatePassword() {
  if (passwordField.value === passwordConfirmField.value)
    if (passwordField.value.length > 5) {
      setValid(passwordField);
      setValid(passwordConfirmField);
      changePassword();
      return true;
    } else {
      setInvalid(passwordField, "Password must be at least 6 characters long!");
      return false;
    }
  else {
    setInvalid(passwordField, "Passwords do not match");
    setInvalid(passwordConfirmField, "");
    return false;
  }
}

function setInvalid(field, message) {
  field.parentNode.className = "form-group col-md-6 has-error";
  field.nextElementSibling.className = "help-block with-errors";
  field.nextElementSibling.innerHTML = message;
}

function setValid(field) {
  field.nextElementSibling.innerHTML = '';
  field.parentNode.className = " form-group col-md-6";
  field.nextElementSibling.className = "help-block";
}
