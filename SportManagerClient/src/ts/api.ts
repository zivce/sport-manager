export default {
    PARTICIPATE_URL: "http://localhost:8080/api/events/participate",
    EVENTS_URL: "http://localhost:8080/api/events/",
    EVENT_TYPES_URL: "http://localhost:8080/api/event-types/",
    USERS_URL: "http://localhost:8080/api/users",
    USER_URL: "http://localhost:8080/users-control-panel/username",
    LOGIN_PROCESS: "http://localhost:8080/login",
    REGISTER_CLIENT_URL: "http://localhost:8080/register",
    CONFIRM_REGISTER_URL: "http://localhost:8080/confirm-register",
    AVATAR_URL: "http://localhost:8080/api/users/changeAvatar/",
    CHANGE_PASSWORD_URL: "http://localhost:8080/api/users/changePassword/",
    SUBSCRIPTIONS_URL: "http://localhost:8080/api/users/subscriptions/",
    CHANGE_CITY: "http://localhost:8080/api/users/changeCity/",
}