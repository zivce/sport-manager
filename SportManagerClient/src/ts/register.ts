import Validation from "./utility/validation";
import * as validate from 'validate.js';
import API from "./api";

const emailField: HTMLInputElement = <HTMLInputElement>document.getElementById('email');
const nameField: HTMLInputElement = <HTMLInputElement>document.getElementById('name');
const passwordField: HTMLInputElement = <HTMLInputElement>document.getElementById('password');
const confirmPasswordField: HTMLInputElement = <HTMLInputElement>document.getElementById('confirmPassword');
const eyeField: HTMLElement = document.getElementById('eye');
const registerForm: HTMLElement = document.getElementById('registerForm');
const locationSelect: HTMLSelectElement = <HTMLSelectElement>document.getElementById('locationSelect');


var constraints = {
	email: {
		// Email is required
		presence: true,
		// and must be an email
		format: {
			pattern: "^[A-Za-z0-9._%+-]+@enjoying.rs$",
			message: "domain must be enjoying.rs"
		}
	},
	name: {
		presence: true,
	},
	password: {
		presence: true,
		length: {
			minimum: 6,
			message: "must be at least 6 characters long",
		}
	},

	confirmPassword: {
		presence: true,
		equality: "password",
	}


};

const submitForm = function (form) {
	// Get from the form
	var values = validate.collectFormValues(form);
	// Validate form against the constraints
	var errors = validate(values, constraints);
	// Update the form 
	Validation.showErrors(form, errors || {});
	if (!errors) {
		let registerData = {
			email: emailField.value.trim(),
			name: nameField.value.trim(),
			password: passwordField.value.trim(),
			confirmPassword: confirmPasswordField.value.trim(),
			location: locationSelect.selectedOptions.item(0).value

		}

		fetch(API.REGISTER_CLIENT_URL,
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(registerData)
			})
			.then(response => response.text())
			.then(response => {
				alert(response);
			})

	}
	return false;
};

registerForm.onsubmit = function () {
	const thisForm = this;
	return submitForm(thisForm);
};

const togglePasswordVisibility = _ => {
	eyeField.classList.toggle('fa-eye-slash');
	eyeField.classList.toggle('fa-eye');
	passwordField.type = passwordField.type === 'text' ? 'password' : 'text';
};

enum Visibility {
	visible = "visible",
	hidden = "hidden"
}

const hideEyeOnEmptyField = function (fieldValue) {
	if (fieldValue) {
		eyeField.style.visibility = Visibility.visible;
	} else {
		eyeField.style.visibility = Visibility.hidden;
	};
};


passwordField.addEventListener('input', function () {
	hideEyeOnEmptyField(passwordField.value.trim());
});

enum MouseEvents {
	mouseover = 'mouseover',
	mouseout = 'mouseout'
}

eyeField.addEventListener(MouseEvents.mouseover, togglePasswordVisibility);

eyeField.addEventListener(MouseEvents.mouseout, togglePasswordVisibility);

const extractNameFromEmail = function (d) {
	let parts = emailField.value.split("@");
	let username = parts.length == 2 ? parts[0] : "";

	let nameParts = username.split(".");
	let fullName = "";
	nameParts.forEach(namePart => {
		namePart = namePart.charAt(0).toUpperCase() + namePart.slice(1);
		fullName += namePart + " ";
	});
	nameField.value = fullName.trim();
}

emailField.addEventListener('focusout', extractNameFromEmail);